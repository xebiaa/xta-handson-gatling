package sut.requests

import io.gatling.core.Predef._
import io.gatling.http.Predef._

trait Login {

    val selectHighestValue = (list: Seq[String]) => {
        list.map(_.toInt).max
    }

    val openStartPage = exec(http("Open the challenge start page.")
        .get("/start")
        .check(status.is(200))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='authenticity_token']", "value").saveAs("authenticityToken"))
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
    )

    val submitStepOne = exec(http("Goto the second step in the challenge.")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParam("commit", "${commitStage}")
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
    )

    val submitStepTwo = exec(http("Goto the third step in the challenge.")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParam("commit", "${commitStage}")
        .formParam("challenger[age]", "20")
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
        .check(css("label.radio").findAll.transform(selectHighestValue).saveAs("largestNumberInList"))
        .check(regex("""value="(.*?)" \/>${largestNumberInList}<\/label>""").saveAs("selectedOrder"))
        .check(css("input[name='authenticity_token']", "value").saveAs("authenticityToken"))
    )

    val submitStepThree = exec(http("Goto the fourth step in the challenge.")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParam("challenger[largest_order]", "${largestNumberInList}")
        .formParam("challenger[order_selected]", "${selectedOrder}")
        .formParam("commit", "${commitStage}")
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
        .check(regex("""input id="challenger_order_.*name="(.*)" type.*value="(.*)"""").ofType[(String, String)].findAll.saveAs("challengeOrderList"))

    )

    val submitStepFour = exec(http("Goto the fifth step in the challenge.")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParamSeq("${challengeOrderList}")
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
        .check(css(".token").saveAs("oneTimeToken")))

    val finishChallenge = exec(http("DONE")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParam("challenger[one_time_token]", "${oneTimeToken}"))

    val getOnetimeCode = exec(http("Retrieve the onetime code to be used in step 5")
        .get("/code")
        .check(jsonPath("$.code").saveAs("oneTimeToken"))
    )

}




package sut.requests

import io.gatling.core.Predef._
import io.gatling.http.Predef._

trait Login {

    val selectHighestValue = (list: Seq[String]) => {
        list.map(_.toInt).max
    }

    val openStartPage = exec(http("Open the challenge start page.")
        .get("/start")
        .check(status.is(200))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='authenticity_token']", "value").saveAs("authenticityToken"))
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
    )

    val submitStepOne = exec(http("Goto the second step in the challenge.")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParam("commit", "${commitStage}")
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
    )

    val submitStepTwo = exec(http("Goto the third step in the challenge.")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParam("commit", "${commitStage}")
        .formParam("challenger[age]", "20")
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
        .check(css("label.radio").findAll.transform(selectHighestValue).saveAs("largestNumberInList"))
        .check(regex("""value="(.*?)" \/>${largestNumberInList}<\/label>""").saveAs("selectedOrder"))
        .check(css("input[name='authenticity_token']", "value").saveAs("authenticityToken"))
    )

    val submitStepThree = exec(http("Goto the fourth step in the challenge.")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParam("challenger[largest_order]", "${largestNumberInList}")
        .formParam("challenger[order_selected]", "${selectedOrder}")
        .formParam("commit", "${commitStage}")
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
        .check(regex("""input id="challenger_order_.*name="(.*)" type.*value="(.*)"""").ofType[(String, String)].findAll.saveAs("challengeOrderList"))

    )

    val submitStepFour = exec(http("Goto the fifth step in the challenge.")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParamSeq("${challengeOrderList}")
        .check(css("#challenger_step_number", "value").saveAs("challengerStepNumber"))
        .check(css("#challenger_step_id", "value").saveAs("challengerStepId"))
        .check(css("input[name='commit']", "value").saveAs("commitStage"))
        .check(css(".token").saveAs("oneTimeToken")))

    val finishChallenge = exec(http("DONE")
        .post("/start")
        .formParam("utf8", "%E2%9C%93")
        .formParam("authenticity_token", "${authenticityToken}")
        .formParam("challenger[step_id]", "${challengerStepId}")
        .formParam("challenger[step_number]", "${challengerStepNumber}")
        .formParam("challenger[one_time_token]", "${oneTimeToken}"))

    val getOnetimeCode = exec(http("Retrieve the onetime code to be used in step 5")
        .get("/code")
        .check(jsonPath("$.code").saveAs("oneTimeToken"))
    )

}




