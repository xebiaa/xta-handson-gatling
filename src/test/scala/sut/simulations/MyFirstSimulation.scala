package sut.simulations

import config.HttpConfig
import feeders.StringFeeder
import io.gatling.core.Predef._
import sut.requests.Login
import scala.concurrent.duration._

class MyFirstSimulation extends Simulation
with Login
with StringFeeder
with HttpConfig{

    val loginScenario = scenario("Flood io challenge")
        .exec(openStartPage)
        .exec(submitStepOne)
        .exec(submitStepTwo)
        .exec(submitStepThree)
        .exec(submitStepFour)
        .exec(getOnetimeCode)
        .exec(finishChallenge)

    setUp(
        loginScenario.inject(
            rampUsers(1) over (10 seconds)
        ).protocols(httpProtocol)
    )
}
