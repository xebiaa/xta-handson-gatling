name := "xta-handson-gatling"

version := "0.1"

scalaVersion := "2.11.4"

val test = project.in(file("."))
  .enablePlugins(GatlingPlugin)
  .settings(libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.1.3" % "test")
  .settings(libraryDependencies += "io.gatling"            % "gatling-test-framework"    % "2.1.3" % "test")
    .settings(libraryDependencies += "io.gatling" % "gatling-bundle" % "2.0.2" % "test" artifacts Artifact("gatling-bundle", "zip", "zip", "bundle"))
