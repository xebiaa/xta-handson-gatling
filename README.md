# xta-handson-gatling

## Installation

Download and install SBT

```
http://www.scala-sbt.org/download.html
```

Clone this repository

```
https://github.com/xebia/xta-handson-gatling.git
```

## Running simulations

You can run all simulations using

```sbt test```

Or a specific simulation

```sbt testOnly MyFirstSimulation.scala```


